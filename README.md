## Usuario

### POST /usuario/cadastrar
Cadastra um novo usuario. O CPF inserido deve ser um CPF valido.

**Request**
```json
{
	"nome": "teste123",
	"cpf": "093.277.830-50",
	"email": "abc@abc.com",
	"dataCadastro": "2020-10-02"
}
```

**Response 201 Created**
```json
{
    "id": 5,
    "nome": "teste123",
    "cpf": "093.277.830-50",
    "email": "abc@abc.com",
    "dataCadastro": "2020-10-02"
}
```

## GET /usuario/listar
Lista todos os usuarios cadastrados

**Response 200**
```json
[
    {
        "id": 1,
        "nome": "Leandro",
        "cpf": "093.277.830-50",
        "email": "abc@abc.com",
        "dataCadastro": "2020-10-01"
    },
    {
        "id": 2,
        "nome": "Saito",
        "cpf": "929.144.790-00",
        "email": "teste@abc.com",
        "dataCadastro": "2020-09-30"
    }
]
```

### POST /usuario/buscar/{id}
Busca um usuario cadastrado pelo id.

**Response 200**
```json
{
	"id": 1,
	"nome": "teste123",
	"cpf": "093.277.830-50",
	"email": "abc@abc.com",
	"dataCadastro": "2020-10-01"
}
```

### PUT /usuario/alterar/{id}
Altera os dados de um usuario já cadastrado.

**Request**
```json
{
	"nome": "Goku",
	"cpf": "929.144.790-00",
	"email": "xyz@xyz.com"
}
```

**Response 200 **
```json
{
    "id": 1,
    "nome": "Goku",
    "cpf": "929.144.790-00",
    "email": "xyz@xyz.com",
    "dataCadastro": "2020-10-02"
}
```

## BatidaPonto

### POST /batidaponto/registrar
Registra uma batida de ponto eletronico. Campo tipoBatida deve ser "entrada" ou "saida"

**Request**
```json
{
    "usuarioId": "1",
    "tipoBatida": "entrada"
}
```

**Response 200 **
```json
{
    "usuario": {
        "id": 1,
        "nome": "Leandro",
        "cpf": "093.277.830-50",
        "email": "abc@abc.com",
        "dataCadastro": "2020-10-01"
    },
    "horario": "2020-02-20T12:05:40.6165958",
    "tipoBatida": "ENTRADA"
}
```

### GET /batidaponto/listar/{id}
Busca todas as batidas de ponto registradas por um usuário e o total de minutos trabalhados. Os minutos trabalhados são contados por duas batidas de ponto nessa ordem: "entrada" e "saida". Batidas de ponto que não seguem esse padrão não são consideradas no cálculo.

**Response 200 **
```json
{
    "listaBatidaPonto": [
        {
            "usuario": {
                "id": 1,
                "nome": "Leandro",
                "cpf": "093.277.830-50",
                "email": "abc@abc.com",
                "dataCadastro": "2020-10-01"
            },
            "horario": "2020-02-19T18:00:21",
            "tipoBatida": "ENTRADA"
        },
        {
            "usuario": {
                "id": 1,
                "nome": "Leandro",
                "cpf": "093.277.830-50",
                "email": "abc@abc.com",
                "dataCadastro": "2020-10-01"
            },
            "horario": "2020-02-19T18:05:45",
            "tipoBatida": "SAIDA"
        }
    ],
    "minutosTrabalhados": 5
}
```

