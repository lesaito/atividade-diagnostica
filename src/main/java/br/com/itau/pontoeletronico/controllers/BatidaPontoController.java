package br.com.itau.pontoeletronico.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.pontoeletronico.dtos.BatidaPontoDTORequest;
import br.com.itau.pontoeletronico.dtos.BatidaPontoDTOResponse;
import br.com.itau.pontoeletronico.models.BatidaPonto;
import br.com.itau.pontoeletronico.models.TipoBatidaPonto;
import br.com.itau.pontoeletronico.services.BatidaPontoService;

@RestController
@RequestMapping("/batidaponto")
public class BatidaPontoController {

	@Autowired
	BatidaPontoService batidaPontoService;
	
	@PostMapping("/registrar")
	public ResponseEntity<BatidaPonto> registrar(@Valid @RequestBody BatidaPontoDTORequest batidaPontoDTORequest) {
		if (batidaPontoDTORequest.getTipoBatida().equals(TipoBatidaPonto.ENTRADA.getTipoBatidaPonto()) || 
				batidaPontoDTORequest.getTipoBatida().equals(TipoBatidaPonto.SAIDA.getTipoBatidaPonto())) {
			
			return new ResponseEntity<BatidaPonto>(batidaPontoService.registrar(batidaPontoDTORequest.getUsuarioId(), batidaPontoDTORequest.getTipoBatida()),HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/listar/{id}")
	public BatidaPontoDTOResponse listar(@PathVariable(value = "id") int usuarioid) {
		Iterable<BatidaPonto> iterableBatidaPonto = batidaPontoService.listar(usuarioid);
		List<BatidaPonto> listaBatidaPonto = new ArrayList<BatidaPonto>();
		iterableBatidaPonto.iterator().forEachRemaining(listaBatidaPonto::add);
		
		long totalMinutosTrabalhados = batidaPontoService.calculaMinutosTrabalhados(listaBatidaPonto);
		
		BatidaPontoDTOResponse batidaPontoDTOResponse = new BatidaPontoDTOResponse();
		batidaPontoDTOResponse.setListaBatidaPonto(iterableBatidaPonto);
		batidaPontoDTOResponse.setMinutosTrabalhados(totalMinutosTrabalhados);

		return batidaPontoDTOResponse;
		
	}
	
}
