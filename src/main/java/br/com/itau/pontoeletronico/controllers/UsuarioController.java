package br.com.itau.pontoeletronico.controllers;

import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import br.com.itau.pontoeletronico.dtos.UsuarioDTORequest;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.services.UsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Usuario> cadastrar(@Valid @RequestBody UsuarioDTORequest usuarioDTORequest) {
		return new ResponseEntity<Usuario>(usuarioService.cadastrar(transformaDTOParaModel(usuarioDTORequest)), HttpStatus.CREATED);
	}
	
	@GetMapping("/listar")
	public Iterable<Usuario> listar() {
		return usuarioService.listar();
	}
	
	@GetMapping(value = "/buscar/{id}")
    public ResponseEntity<Usuario> GetById(@PathVariable(value = "id") int id) {
        Optional<Usuario> usuario = usuarioService.buscar(id);
        if(usuario.isPresent())
            return new ResponseEntity<Usuario>(usuario.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
	
	@PutMapping(value = "/alterar/{id}")
    public ResponseEntity<Usuario> alterar(@PathVariable(value = "id") int id, @Valid @RequestBody UsuarioDTORequest novoUsuarioDTO) {
        Usuario novoUsuario = transformaDTOParaModel(novoUsuarioDTO);
		Optional<Usuario> antigoUsuario = usuarioService.buscar(id);
        if(antigoUsuario.isPresent()){
            Usuario usuario = antigoUsuario.get();
            usuario.setNome(novoUsuario.getNome());
            usuario.setCpf(novoUsuario.getCpf());
            usuario.setEmail(novoUsuario.getEmail());
            usuarioService.cadastrar(usuario);
            return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
	
	private Usuario transformaDTOParaModel(UsuarioDTORequest usuarioDTORequest) {
		return new Usuario(usuarioDTORequest.getNome(), usuarioDTORequest.getCpf(), usuarioDTORequest.getEmail(), usuarioDTORequest.getDataCadastro());
	}
}