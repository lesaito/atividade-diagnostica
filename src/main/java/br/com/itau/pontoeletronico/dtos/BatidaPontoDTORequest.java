package br.com.itau.pontoeletronico.dtos;

public class BatidaPontoDTORequest {
	
	private int usuarioId;
	private String tipoBatida;
	
	public int getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
	public String getTipoBatida() {
		return tipoBatida;
	}
	public void setTipoBatida(String tipoBatida) {
		this.tipoBatida = tipoBatida;
	}
}
