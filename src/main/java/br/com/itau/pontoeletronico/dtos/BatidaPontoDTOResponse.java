package br.com.itau.pontoeletronico.dtos;

import br.com.itau.pontoeletronico.models.BatidaPonto;

public class BatidaPontoDTOResponse {

	private Iterable<BatidaPonto> listaBatidaPonto;
	
	private long minutosTrabalhados;

	public Iterable<BatidaPonto> getListaBatidaPonto() {
		return listaBatidaPonto;
	}

	public void setListaBatidaPonto(Iterable<BatidaPonto> listaBatidaPonto) {
		this.listaBatidaPonto = listaBatidaPonto;
	}

	public long getMinutosTrabalhados() {
		return minutosTrabalhados;
	}

	public void setMinutosTrabalhados(long minutosTrabalhados) {
		this.minutosTrabalhados = minutosTrabalhados;
	}
}
