package br.com.itau.pontoeletronico.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class BatidaPonto {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private Usuario usuario;
	
	@Column(nullable=false)
	private TipoBatidaPonto tipoBatidaPonto;
	
	@Column(nullable = false)
	private LocalDateTime horario;
	
	public BatidaPonto() {
		
	}
	
	public BatidaPonto(Usuario usuario, TipoBatidaPonto tipoBatidaPonto) {
		this.usuario = usuario;
		this.tipoBatidaPonto = tipoBatidaPonto;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setIdUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoBatidaPonto getTipoBatida() {
		return this.tipoBatidaPonto;
	}

	public void setTipoBatida(TipoBatidaPonto tipoBatida) {
		this.tipoBatidaPonto = tipoBatida;
	}

	public LocalDateTime getHorario() {
		return this.horario;
	}

	public void setHorario(LocalDateTime horario) {
		this.horario = horario;
	}	
}