package br.com.itau.pontoeletronico.models;

public enum TipoBatidaPonto 
{
    ENTRADA("entrada"), 
    SAIDA("saida");
    
    private String tipoBatidaPonto;
 
	TipoBatidaPonto(String tipoBatidaPonto) {
        this.tipoBatidaPonto = tipoBatidaPonto;
    }
 
    public String getTipoBatidaPonto() {
        return tipoBatidaPonto;
    }
}