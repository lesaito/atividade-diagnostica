package br.com.itau.pontoeletronico.models;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.br.CPF;

@Entity
public class Usuario {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private int id;
	
	@NotBlank(message="Nome vazio")
	@Column(nullable = false)
	private String nome;
	
	@CPF
	@NotBlank(message="CPF vazio")
	@Column(nullable = false)
	private String cpf;
		
	@Pattern(regexp=".+@.+\\..+", message="Por favor insira um email valido")
	@NotBlank(message="Email vazio")
	@Column(nullable = false)
	private String email;
	
	@Column(nullable = false)
	private LocalDate dataCadastro;
	
	public Usuario() {
		
	}
	
	public Usuario(String nome, String cpf, String email, LocalDate dataCadastro) {
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.dataCadastro = dataCadastro;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}	
}