package br.com.itau.pontoeletronico.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.pontoeletronico.models.BatidaPonto;
import br.com.itau.pontoeletronico.models.Usuario;

public interface BatidaPontoRepository extends CrudRepository<BatidaPonto, Integer> {

	public Iterable<BatidaPonto> findBatidaPontoByUsuario(Usuario usuario);
}
