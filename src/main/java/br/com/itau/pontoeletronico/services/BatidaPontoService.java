package br.com.itau.pontoeletronico.services;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.pontoeletronico.models.BatidaPonto;
import br.com.itau.pontoeletronico.models.TipoBatidaPonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.BatidaPontoRepository;

@Service
public class BatidaPontoService {
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private BatidaPontoRepository batidaPontoRepository;
	
	public BatidaPonto registrar(int usuarioId, String tipoBatida) {
		Optional<Usuario> usuario = usuarioService.buscar(usuarioId);
		
		BatidaPonto batidaPonto = new BatidaPonto();
		batidaPonto.setIdUsuario(usuario.get());
		if (tipoBatida.equals(TipoBatidaPonto.ENTRADA.getTipoBatidaPonto())) {
			batidaPonto.setTipoBatida(TipoBatidaPonto.ENTRADA);	
		} else {
			batidaPonto.setTipoBatida(TipoBatidaPonto.SAIDA);
		}		
		batidaPonto.setHorario(LocalDateTime.now());
		
		return batidaPontoRepository.save(batidaPonto);
	}
	
	public Iterable<BatidaPonto> listar(int usuarioId) {
		Optional<Usuario> usuario = usuarioService.buscar(usuarioId);
		return batidaPontoRepository.findBatidaPontoByUsuario(usuario.get());
	}
	
	public Long calculaMinutosTrabalhados(List<BatidaPonto> listaBatidaPonto) {
		long totalMinutosTrabalhados = 0;
		
		for (int i = 0; i < listaBatidaPonto.size(); i++) {
			
			if(listaBatidaPonto.get(i).getTipoBatida().equals(TipoBatidaPonto.ENTRADA)) {
				for (int j = i+1; j < listaBatidaPonto.size(); j++) {
					if(listaBatidaPonto.get(j).getTipoBatida().equals(TipoBatidaPonto.SAIDA)) {
						totalMinutosTrabalhados += calculaMinutos(listaBatidaPonto.get(i).getHorario(), listaBatidaPonto.get(j).getHorario());
						i = j;
						break;
					}
				}
			}
				
		}
		 
		return totalMinutosTrabalhados;
	}
	
	private Long calculaMinutos(LocalDateTime entrada, LocalDateTime saida) {
		return ChronoUnit.MINUTES.between(entrada, saida);
	}
	
}
