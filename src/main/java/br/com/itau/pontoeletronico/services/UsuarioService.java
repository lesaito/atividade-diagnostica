package br.com.itau.pontoeletronico.services;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.UsuarioRepository;

@Service
public class UsuarioService {
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public Usuario cadastrar (Usuario usuario) {		
		return usuarioRepository.save(usuario);
	}
	
	public Iterable<Usuario> listar() {
		return usuarioRepository.findAll();
	}

	public Optional<Usuario> buscar(int id) {
		return usuarioRepository.findById(id);
	}
}
