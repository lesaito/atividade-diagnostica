package br.com.itau.pontoeletronico.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.itau.pontoeletronico.models.BatidaPonto;
import br.com.itau.pontoeletronico.models.TipoBatidaPonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.services.BatidaPontoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = BatidaPontoController.class)
public class BatidaPontoControllerTest {

	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private BatidaPontoService batidaPontoService;
	
	private BatidaPonto batidaPontoEntrada;
	private BatidaPonto batidaPontoSaida;
	
	private Usuario usuario;
	
	@Before
	public void preparar() {
		usuario = new Usuario();
	    usuario.setId(1);
	    usuario.setNome("Jose");
	    usuario.setCpf("093.277.830-50");
	    usuario.setEmail("abc@abc.com");
	    usuario.setDataCadastro(LocalDate.parse("2020-10-02"));
		
		batidaPontoEntrada = new BatidaPonto();
		batidaPontoEntrada.setIdUsuario(usuario);
		batidaPontoEntrada.setTipoBatida(TipoBatidaPonto.ENTRADA);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		batidaPontoEntrada.setHorario(LocalDateTime.parse("2020-10-02 10:00", formatter));
		
		batidaPontoSaida = new BatidaPonto();
		batidaPontoSaida.setIdUsuario(usuario);
		batidaPontoSaida.setTipoBatida(TipoBatidaPonto.SAIDA);
		batidaPontoSaida.setHorario(LocalDateTime.parse("2020-10-02 12:00", formatter));
	}
	
	@Test
	public void deveRegistrarBatidaDePontoEntrada() throws Exception {
		when(batidaPontoService.registrar(Mockito.any(int.class), Mockito.any(String.class))).thenReturn(batidaPontoEntrada);
		
		String batidaPontoJson = "{\"usuarioId\":\"1\",\"tipoBatida\":\"entrada\"}";
		
		mockMvc.perform(post("/batidaponto/registrar")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(batidaPontoJson))
		.andExpect(status().isOk())
		.andExpect(content().string(containsString("1")))
		.andExpect(content().string(containsString("ENTRADA")));	
	}
	
	@Test
	public void deveRegistrarBatidaDePontoSaida() throws Exception {
		when(batidaPontoService.registrar(Mockito.any(int.class), Mockito.any(String.class))).thenReturn(batidaPontoSaida);
		
		String batidaPontoJson = "{\"usuarioId\":\"1\",\"tipoBatida\":\"saida\"}";
		
		mockMvc.perform(post("/batidaponto/registrar")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(batidaPontoJson))
		.andExpect(status().isOk())
		.andExpect(content().string(containsString("1")))
		.andExpect(content().string(containsString("SAIDA")));	
	}
	
	@Test
	public void deveRegistrarBatidaDePontoInvalida() throws Exception {	
		String batidaPontoJson = "{\"usuarioId\":\"1\",\"tipoBatida\":\"macarrao\"}";
		
		mockMvc.perform(post("/batidaponto/registrar")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(batidaPontoJson))
		.andExpect(status().isInternalServerError());	
	}
	
	@Test
	public void deveListarBatidasDePontoETotalDeMinutosTrabalhados() throws Exception {
		when(batidaPontoService.listar(Mockito.any(int.class))).thenReturn(Collections.emptyList());
		when(batidaPontoService.calculaMinutosTrabalhados(Collections.emptyList())).thenReturn((long) 65);
		
		mockMvc.perform(get("/batidaponto/listar/1"))
		.andExpect(status().isOk())
		.andExpect(content().string(containsString("65")));	
	}
}
