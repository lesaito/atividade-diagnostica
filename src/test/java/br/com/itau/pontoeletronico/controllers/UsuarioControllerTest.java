package br.com.itau.pontoeletronico.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.services.UsuarioService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UsuarioController.class)
public class UsuarioControllerTest {
	
	@Autowired
	private MockMvc mockMvc;	
	@MockBean
	private UsuarioService usuarioService;
	
	private Usuario usuario;
	
	@Before
	public void preparar() {
	    usuario = new Usuario();
	    
	    usuario.setNome("Jose");
	    usuario.setCpf("093.277.830-50");
	    usuario.setEmail("abc@abc.com");
	    usuario.setDataCadastro(LocalDate.parse("2020-10-02"));
	}
	
	 @Test
	 public void deveCadastrarUmUsuario() throws Exception {
		 when(usuarioService.cadastrar(any(Usuario.class))).thenReturn(usuario);
		 String usuarioJson = "{\"nome\":\"Jose\",\"cpf\":\"400.400.400-40\",\"email\":\"abc@abc.com\",\"dataCadastro\":\"2020-10-02\"}";
		 
		 mockMvc.perform(post("/usuario/cadastrar")
				 .contentType(MediaType.APPLICATION_JSON_UTF8)
				 .content(usuarioJson))
		 .andExpect(status().isCreated())
		 .andExpect(content().string(containsString("Jose")))
		 .andExpect(content().string(containsString("093.277.830-50")))
		 .andExpect(content().string(containsString("abc@abc.com")))
		 .andExpect(content().string(containsString("2020-10-02")));
	 }
	 
	 @Test
	 public void deveConsultarListaDeUsuarios() throws Exception {
		 when(usuarioService.listar()).thenReturn(Collections.emptyList());
		 
		 mockMvc.perform(get("/usuario/listar"))
				 .andExpect(status().isOk());
	 }
	 
	 @Test
	 public void deveBuscarUsuarioPorId() throws Exception {
		 when(usuarioService.buscar(any(int.class))).thenReturn(Optional.of(usuario));
		 
		 mockMvc.perform(get("/usuario/buscar/1"))
		 .andExpect(status().isOk())
		 .andExpect(content().string(containsString("Jose")))
		 .andExpect(content().string(containsString("093.277.830-50")))
		 .andExpect(content().string(containsString("abc@abc.com")))
		 .andExpect(content().string(containsString("2020-10-02")));
	 }
	 
	 @Test
	 public void deveNaoEncontrarUsuarioNaBusca() throws Exception {
		 when(usuarioService.buscar(any(int.class))).thenReturn(Optional.empty());
		 
		 mockMvc.perform(get("/usuario/buscar/1"))
		 .andExpect(status().isNotFound());
	 }
	 
	 @Test
	 public void deveAlterarUmUsuario() throws Exception {
		 when(usuarioService.buscar(any(int.class))).thenReturn(Optional.of(usuario));
		 
		 String usuarioJson = "{\"nome\":\"Ronaldo\",\"cpf\":\"929.144.790-00\",\"email\":\"cde@cde.com\"}";
		 
		 mockMvc.perform(put("/usuario/alterar/1")
				 .contentType(MediaType.APPLICATION_JSON_UTF8)
				 .content(usuarioJson))
		 .andExpect(status().isOk())
		 .andExpect(content().string(containsString("Ronaldo")))
		 .andExpect(content().string(containsString("929.144.790-00")))
		 .andExpect(content().string(containsString("cde@cde.com")))
		 .andExpect(content().string(containsString("2020-10-02")));
	 }
	 
	 @Test
	 public void deveNaoEncontrarUsuarioParaAlterar() throws Exception {
		 when(usuarioService.buscar(any(int.class))).thenReturn(Optional.empty());
		 
		 String usuarioJson = "{\"nome\":\"Ronaldo\",\"cpf\":\"929.144.790-00\",\"email\":\"cde@cde.com\"}";
		 
		 mockMvc.perform(put("/usuario/alterar/1")
				 .contentType(MediaType.APPLICATION_JSON_UTF8)
				 .content(usuarioJson))
		 .andExpect(status().isNotFound());
	 }
}
