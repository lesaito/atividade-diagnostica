package br.com.itau.pontoeletronico.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.pontoeletronico.models.BatidaPonto;
import br.com.itau.pontoeletronico.models.TipoBatidaPonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.BatidaPontoRepository;
import br.com.itau.pontoeletronico.repositories.UsuarioRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {BatidaPontoService.class, UsuarioService.class})
public class BatidaPontoServiceTest {

	@MockBean
	private BatidaPontoRepository batidaPontoRepository;
	
	@MockBean
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private BatidaPontoService batidaPontoService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	private BatidaPonto batidaPontoEntrada;
	private BatidaPonto batidaPontoSaida;
	private List<BatidaPonto> listaBatidaPonto;
	private Usuario usuario;
	
	@Before
	public void inicializar() {
		usuario = new Usuario();
	    usuario.setId(1);
	    usuario.setNome("Jose");
	    usuario.setCpf("093.277.830-50");
	    usuario.setEmail("abc@abc.com");
	    usuario.setDataCadastro(LocalDate.parse("2020-10-02"));
		
		batidaPontoEntrada = new BatidaPonto();
		batidaPontoEntrada.setIdUsuario(usuario);
		batidaPontoEntrada.setTipoBatida(TipoBatidaPonto.ENTRADA);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		batidaPontoEntrada.setHorario(LocalDateTime.parse("2020-10-02 10:00", formatter));
		
		batidaPontoSaida = new BatidaPonto();
		batidaPontoSaida.setIdUsuario(usuario);
		batidaPontoSaida.setTipoBatida(TipoBatidaPonto.SAIDA);
		batidaPontoSaida.setHorario(LocalDateTime.parse("2020-10-02 12:00", formatter));
		
		listaBatidaPonto = new ArrayList<BatidaPonto>();
		listaBatidaPonto.add(batidaPontoEntrada);
		listaBatidaPonto.add(batidaPontoSaida);
	}
	
	@Test
	public void testarRegistroBatidaPontoEntrada() {
		when(usuarioService.buscar(1)).thenReturn(Optional.of(usuario));
		when(batidaPontoRepository.save(Mockito.any(BatidaPonto.class))).thenReturn(batidaPontoEntrada);
		
		BatidaPonto registroPonto = batidaPontoService.registrar(1, "entrada");
		
		assertEquals(registroPonto.getUsuario().getNome(), usuario.getNome());
		assertEquals(registroPonto.getUsuario().getCpf(), usuario.getCpf());
		assertEquals(registroPonto.getUsuario().getEmail(), usuario.getEmail());
		assertEquals(registroPonto.getUsuario().getDataCadastro(), usuario.getDataCadastro());
		assertEquals(registroPonto.getTipoBatida().getTipoBatidaPonto(), "entrada");
	}
	
	@Test
	public void testarRegistroBatidaPontoSaida() {
		when(usuarioService.buscar(1)).thenReturn(Optional.of(usuario));
		when(batidaPontoRepository.save(Mockito.any(BatidaPonto.class))).thenReturn(batidaPontoSaida);
		
		BatidaPonto registroPonto = batidaPontoService.registrar(1, "saida");
		
		assertEquals(registroPonto.getUsuario().getNome(), usuario.getNome());
		assertEquals(registroPonto.getUsuario().getCpf(), usuario.getCpf());
		assertEquals(registroPonto.getUsuario().getEmail(), usuario.getEmail());
		assertEquals(registroPonto.getUsuario().getDataCadastro(), usuario.getDataCadastro());
		assertEquals(registroPonto.getTipoBatida().getTipoBatidaPonto(), "saida");
	}
	
	@Test
	public void testarListaDeBatidasDePontoPorUsuario() {
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));
		when(batidaPontoRepository.findBatidaPontoByUsuario(usuario)).thenReturn(Collections.emptyList());
		
		Iterable<BatidaPonto> listaBatidaPonto = batidaPontoService.listar(1);
		
		assertEquals(listaBatidaPonto, Collections.emptyList());
	}
	
	@Test
	public void testarCalculoMinutosTrabalhados() {
		long minutosTrabalhados = 120;
		assertEquals(minutosTrabalhados, (long) batidaPontoService.calculaMinutosTrabalhados(listaBatidaPonto));
	}
}
