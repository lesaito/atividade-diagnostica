package br.com.itau.pontoeletronico.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.UsuarioRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UsuarioService.class)
public class UsuarioServiceTest {
	@MockBean
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private UsuarioService usuarioService;
	
	private Usuario usuario;
	
	@Before
	public void inicializar() {
		usuario = new Usuario();
	    
		usuario.setId(1);
	    usuario.setNome("Jose");
	    usuario.setCpf("093.277.830-50");
	    usuario.setEmail("abc@abc.com");
	    usuario.setDataCadastro(LocalDate.parse("2020-10-02"));
	}
	
	@Test
	public void testarCadastroDeUsuario() {
		when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
		Usuario novoUsuario = usuarioService.cadastrar(usuario);
		
		assertEquals(novoUsuario.getNome(), usuario.getNome());
		assertEquals(novoUsuario.getCpf(), usuario.getCpf());
		assertEquals(novoUsuario.getEmail(), usuario.getEmail());
		assertEquals(novoUsuario.getDataCadastro(), usuario.getDataCadastro());
	}
	
	@Test
	public void testarListarUsuarios() {
		when(usuarioRepository.findAll()).thenReturn(Collections.emptyList());
		Iterable<Usuario> listaUsuarios = usuarioService.listar(); 
		
		assertEquals(listaUsuarios, Collections.emptyList());
	}
	
	@Test
	public void testarBuscaUsuario() {
		when(usuarioRepository.findById(Mockito.any(int.class))).thenReturn(Optional.of(usuario));
		Optional<Usuario> buscaUsuario = usuarioService.buscar(1); 
		
		assertEquals(buscaUsuario.get().getNome(), usuario.getNome());
		assertEquals(buscaUsuario.get().getCpf(), usuario.getCpf());
		assertEquals(buscaUsuario.get().getEmail(), usuario.getEmail());
		assertEquals(buscaUsuario.get().getDataCadastro(), usuario.getDataCadastro());
	}

}
